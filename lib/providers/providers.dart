import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:farmturno/config/theme.dart';
import 'package:farmturno/models/farmacia_turno_model.dart';
import 'package:farmturno/services/preferences_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final colorsProvider = Provider<List<Color>>((ref) {
  return colorList;
});

final selectedColorProvider = StateProvider<int>((ref) {
  final selected = PreferenceService.getString('selectedColor') ?? '';

  if (selected.isEmpty) return 0;

  return int.parse(selected);
});

final isDarkModeProvider = StateProvider<bool>((ref) {
  final isDarkMode = PreferenceService.getBool('isDarkMode');
  return isDarkMode ?? false;
});

final selectedFarmaciaProvider =
    FutureProvider.family<FarmaciasTurno?, String>((ref, id) async {
  final dio = Dio();
  final response = await dio.get(
    'https://midas.minsal.cl/farmacia_v2/WS/getLocalesTurnos.php',
  );
  final List<FarmaciasTurno> farmacias = [];
  for (final farmacia in json.decode(response.data) ?? []) {
    farmacias.add(FarmaciasTurno.fromJson(farmacia));
  }

  for (final farmacia in farmacias) {
    if (farmacia.localId == id) return farmacia;
  }
  return null;
});

final farmaciasQueryProvider = FutureProvider.family
    .autoDispose<List<FarmaciasTurno>, String>((ref, query) async {
  if (query.isEmpty) return [];
  List<FarmaciasTurno> farmacias = [];

  final farmaciasData = await ref.watch(farmaciasProvider.future);

  farmacias = [...farmacias, ...farmaciasData];

  if (farmacias.isEmpty) return [];

  return farmacias = farmacias
      .where((element) =>
          element.fkRegion == '8' &&
          element.localNombre.toLowerCase().contains(query.toLowerCase()))
      .toList();
});

final farmaciasProvider = FutureProvider<List<FarmaciasTurno>>((ref) async {
  final dio = Dio();
  final response = await dio.get(
    'https://midas.minsal.cl/farmacia_v2/WS/getLocalesTurnos.php',
  );
  final List<FarmaciasTurno> farmacias = [];
  for (final farmacia in json.decode(response.data) ?? []) {
    farmacias.add(FarmaciasTurno.fromJson(farmacia));
  }

  return farmacias;
});

final preferenceServiceProvider = Provider<PreferenceService>((ref) {
  return PreferenceService();
});
