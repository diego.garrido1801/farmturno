// To parse this JSON data, do
//
//     final farmaciasTurno = farmaciasTurnoFromJson(jsonString);

import 'dart:convert';

class FarmaciasTurno {
  final DateTime fecha;
  final String localId;
  final String fkRegion;
  final String fkComuna;
  final String fkLocalidad;
  final String localNombre;
  final String comunaNombre;
  final String localidadNombre;
  final String localDireccion;
  final String funcionamientoHoraApertura;
  final String funcionamientoHoraCierre;
  final String localTelefono;
  final String localLat;
  final String localLng;
  final String funcionamientoDia;

  FarmaciasTurno({
    required this.fecha,
    required this.localId,
    required this.fkRegion,
    required this.fkComuna,
    required this.fkLocalidad,
    required this.localNombre,
    required this.comunaNombre,
    required this.localidadNombre,
    required this.localDireccion,
    required this.funcionamientoHoraApertura,
    required this.funcionamientoHoraCierre,
    required this.localTelefono,
    required this.localLat,
    required this.localLng,
    required this.funcionamientoDia,
  });

  factory FarmaciasTurno.fromRawJson(String str) =>
      FarmaciasTurno.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FarmaciasTurno.fromJson(Map<String, dynamic> json) => FarmaciasTurno(
        fecha: DateTime.parse(json["fecha"]),
        localId: json["local_id"],
        fkRegion: json["fk_region"],
        fkComuna: json["fk_comuna"],
        fkLocalidad: json["fk_localidad"],
        localNombre: json["local_nombre"],
        comunaNombre: json["comuna_nombre"],
        localidadNombre: json["localidad_nombre"],
        localDireccion: json["local_direccion"],
        funcionamientoHoraApertura: json["funcionamiento_hora_apertura"],
        funcionamientoHoraCierre: json["funcionamiento_hora_cierre"],
        localTelefono: json["local_telefono"],
        localLat: json["local_lat"],
        localLng: json["local_lng"],
        funcionamientoDia: json["funcionamiento_dia"],
      );

  Map<String, dynamic> toJson() => {
        "fecha":
            "${fecha.year.toString().padLeft(4, '0')}-${fecha.month.toString().padLeft(2, '0')}-${fecha.day.toString().padLeft(2, '0')}",
        "local_id": localId,
        "fk_region": fkRegion,
        "fk_comuna": fkComuna,
        "fk_localidad": fkLocalidad,
        "local_nombre": localNombre,
        "comuna_nombre": comunaNombre,
        "localidad_nombre": localidadNombre,
        "local_direccion": localDireccion,
        "funcionamiento_hora_apertura": funcionamientoHoraApertura,
        "funcionamiento_hora_cierre": funcionamientoHoraCierre,
        "local_telefono": localTelefono,
        "local_lat": localLat,
        "local_lng": localLng,
        "funcionamiento_dia": funcionamientoDia,
      };
}
