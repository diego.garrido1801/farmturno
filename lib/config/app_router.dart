import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:farmturno/screens/farmacia_page.dart';
import 'package:farmturno/screens/home_page.dart';
import 'package:farmturno/screens/no_internet_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

final goRouterProvider = Provider((ref) {
  final goRouterNotifier = ref.read(goRouterNotifierProvider);

  return GoRouter(
    initialLocation: '/',
    refreshListenable: goRouterNotifier,
    routes: [
      ///* Primera pantalla
      GoRoute(
          path: '/',
          builder: (context, state) => const HomeScreen(),
          routes: [
            GoRoute(
              path: 'farmacia/:id',
              builder: (context, state) => FarmaciaScreen(
                indexFarmacia: state.pathParameters['id'] ?? 'no-id',
              ),
            ),
          ]),
      GoRoute(
        path: '/loading',
        builder: (context, state) => const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
      GoRoute(
        path: '/no-connection',
        builder: (context, state) => const NoInternetScreen(),
      ),
    ],
    redirect: (context, state) {
      final goingTo = state.location;
      final connectionStatus = goRouterNotifier.connectionStatus;

      if (connectionStatus == ConnectionState.waiting) {
        return '/loading';
      }

      if (connectionStatus == ConnectionState.none) {
        return '/no-connection';
      }

      if (goingTo == '/loading' || goingTo == '/no-connection') return '/';
      return null;
    },
  );
});

final goRouterNotifierProvider = Provider((ref) {
  final connectionChecker = Connectivity();
  return GoRouterNotifier(connectionChecker);
});

class GoRouterNotifier extends ChangeNotifier {
  final Connectivity _connectionChecker;

  ConnectionState _connectionStatus = ConnectionState.waiting;

  GoRouterNotifier(this._connectionChecker) {
    _connectionChecker.onConnectivityChanged.listen((event) async {
      final connection = await InternetConnectionChecker().hasConnection;

      if (connection) {
        connectionStatus = ConnectionState.active;
      } else {
        connectionStatus = ConnectionState.none;
      }
    });
  }

  ConnectionState get connectionStatus => _connectionStatus;

  set connectionStatus(ConnectionState value) {
    _connectionStatus = value;
    notifyListeners();
  }
}
