import 'package:farmturno/delegates/farmacias_delegate.dart';
import 'package:farmturno/providers/providers.dart';
import 'package:farmturno/services/preferences_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends ConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final colors = Theme.of(context).colorScheme;
    final farmaciasTurno = ref.watch(farmaciasProvider);
    final coloresDisponibles = ref.watch(colorsProvider);
    final selectedcolor = ref.watch(selectedColorProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Farmacias de hoy',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: colors.primary,
        foregroundColor: Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              showSearch(
                context: context,
                delegate: FarmaciasDelegate(),
              );
            },
            icon: const Icon(
              Icons.search,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {
              ref.invalidate(farmaciasProvider);
            },
            icon: const Icon(
              Icons.refresh_sharp,
              color: Colors.white,
            ),
          )
        ],
      ),
      drawer: NavigationDrawer(
        children: [
          const NavigationDrawerDestination(
            label: Text('Farmacias'),
            icon: Icon(Icons.health_and_safety),
          ),
          const SizedBox(
            height: 20,
          ),
          const Divider(),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text('Elección de tema'),
          ),
          SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: ListView.builder(
                itemCount: coloresDisponibles.length,
                itemBuilder: (context, index) {
                  final color = coloresDisponibles[index];
                  return RadioListTile(
                    title: Text('Option ${index + 1}'),
                    subtitle: Text('Color: ${color.value}'),
                    activeColor: color,
                    value: index,
                    groupValue: selectedcolor,
                    onChanged: (value) {
                      PreferenceService.saveString(
                          'selectedColor', index.toString());
                      ref.read(selectedColorProvider.notifier).state = index;
                    },
                  );
                },
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
      body: farmaciasTurno.when(
          data: (data) => ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  final farmaciaActual = data[index];
                  return farmaciaActual.fkRegion == '8'
                      ? InkWell(
                          onTap: () => context
                              .push('/farmacia/${farmaciaActual.localId}'),
                          child: Container(
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            height: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 3,
                                  blurRadius: 7,
                                  offset: const Offset(0, 3),
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(height: 10),
                                Icon(
                                  Icons.health_and_safety,
                                  size: 50,
                                  color: colors.primary,
                                ),
                                const SizedBox(height: 10),
                                Text(
                                  farmaciaActual.localNombre.length > 15
                                      ? '${farmaciaActual.localNombre.substring(0, 15)}...'
                                      : farmaciaActual.localNombre,
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const Spacer(),
                                FilledButton.tonal(
                                  onPressed: () {},
                                  child: const Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.arrow_forward_ios_outlined,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      : const SizedBox();
                },
              ),
          error: (error, __) => const Center(
                child: Text('Error al recuperar las farmacias'),
              ),
          loading: () => const Center(
                child: CircularProgressIndicator(),
              )),
    );
  }
}
