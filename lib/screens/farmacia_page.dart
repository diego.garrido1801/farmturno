import 'package:farmturno/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:url_launcher/url_launcher.dart';

class FarmaciaScreen extends ConsumerWidget {
  const FarmaciaScreen({super.key, required this.indexFarmacia});
  final String indexFarmacia;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final colors = Theme.of(context).colorScheme;

    final farmaciaTurno = ref.watch(selectedFarmaciaProvider(indexFarmacia));

    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalles de la farmacia'),
        backgroundColor: colors.secondary,
        foregroundColor: Colors.white,
      ),
      body: farmaciaTurno.when(
        loading: () => const Center(
          child: CircularProgressIndicator(),
        ),
        error: (_, __) => const Center(
          child: Text('No se ha podido cargar la farmacia de turno'),
        ),
        data: (data) => Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Icon(
                  Icons.health_and_safety_outlined,
                  size: 80,
                  color: colors.primary,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Nombre de la farmacia: ${data?.localNombre}',
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Text('Hora de cierre: ${data?.funcionamientoHoraCierre}'),
              const SizedBox(height: 10),
              Text('Dirección: ${data?.localDireccion}'),
              const SizedBox(height: 10),
              Text('Teléfono: ${data?.localTelefono}'),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      _launchURL('tel:${data?.localTelefono}');
                    },
                    child: const Text('Llamar farmacia'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      _launchURL(
                          'geo:${data?.localLat},${data?.localLng}?z=16&q=${data?.localLat},${data?.localLng}');
                    },
                    child: const Text('Ver ubicación'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _launchURL(String url) async {
    Uri uri = Uri.parse(url);
    await launchUrl(uri);
  }
}
