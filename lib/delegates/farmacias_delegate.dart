import 'package:farmturno/models/farmacia_turno_model.dart';
import 'package:farmturno/providers/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

class FarmaciasDelegate extends SearchDelegate<FarmaciasTurno?> {
  FarmaciasDelegate();
  @override
  String get searchFieldLabel => 'Buscar farmacia';

  @override
  List<Widget>? buildActions(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    return [
      if (query.isNotEmpty)
        IconButton(
          onPressed: () => query = '',
          icon: Icon(
            Icons.highlight_remove_outlined,
            color: colors.primary,
          ),
        )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    return IconButton(
      onPressed: () => close(context, null),
      icon: Icon(
        Icons.arrow_back_ios_new_outlined,
        color: colors.primary,
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    return Consumer(
      builder: (context, ref, child) {
        final farmaciasQuery = ref.watch(farmaciasQueryProvider(query));
        return farmaciasQuery.when(
          data: (data) {
            if (data.isEmpty) {
              return const Center(
                  child: Text('No hay farmacias con ese nombre'));
            }
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                final farmacia = data[index];
                return ListTile(
                  title: Text(farmacia.localNombre),
                  leading: Icon(
                    Icons.health_and_safety_outlined,
                    color: colors.primary,
                  ),
                  onTap: () => context.push('/farmacia/${farmacia.localId}'),
                );
              },
            );
          },
          error: (error, _) {
            return const Center(
              child: Text('No se ha podido obtener las farmacias'),
            );
          },
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return const Text('');
  }
}
